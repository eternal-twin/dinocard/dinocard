<?php

namespace App\Challenge\Entity;
use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\ElementType;
use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\FilterOperator;

/**
 * Abstract Filter (will filter an array of targets)
 */
abstract class TargetFilter
{
    /**
     * @param array|Dinoz[] $targets
     *
     * @return array|Dinoz[]
     */
    public function filterDinoz(array $targets): array
    {
        return $targets;
    }

    /**
     * @param array|Card[] $targets
     *
     * @return array|Card[]
     */
    public function filterCards(array $targets): array
    {
        return $targets;
    }

    /**
     * Parse the provided array and generates the appropriate TargetFilter
     * @param $filterJson
     *
     * @return TargetFilter|null
     */
    public static function initialize($filterJson): ?TargetFilter
    {
        $targetFilter = null;

        if (!is_array($filterJson)) {
            $filterJson = array($filterJson);
        }

        if (array_key_exists("keyword", $filterJson)) {
            $keyword = Keyword::from($filterJson["keyword"]);
            $targetFilter = new KeywordFilter($keyword, array_key_exists("inverted", $filterJson));
        } elseif (array_key_exists("element", $filterJson)) {
            $element = ElementType::from($filterJson["element"]);
            $targetFilter = new ElementFilter($element, array_key_exists("inverted", $filterJson));
        } elseif (array_key_exists("stat", $filterJson)) {
            $stat = DinocardStat::from($filterJson["stat"]);
            $operator = FilterOperator::from($filterJson["operator"]);
            $value = 0;
            if (array_key_exists("value", $filterJson)) {
                $value = $filterJson["value"];
            }
            $targetFilter = new StatFilter($stat, $operator, $value);
        }

        return $targetFilter;
    }
}
