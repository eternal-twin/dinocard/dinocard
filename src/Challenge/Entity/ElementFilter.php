<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\ElementType;

/**
 * Filter any Target by their Element
 */
class ElementFilter extends TargetFilter
{
    private bool $isInverted;
    private ElementType $searchedElement;

    /**
     * @param ElementType $searchedElement
     * @param bool        $inverted
     */
    public function __construct(ElementType $searchedElement, bool $inverted = false)
    {
        $this->searchedElement = $searchedElement;
        $this->isInverted = $inverted;
    }

    /**
     * @param array $targets
     *
     * @return array|Dinoz[]
     */
    public function filterDinoz(array $targets): array
    {
        $filter = fn ($dinoz) => !$this->isInverted === ($dinoz->getOwningCard()->getElement() !== $this->searchedElement);

        return array_filter($targets, $filter);
    }

    /**
     * @param array $targets
     *
     * @return array|Card[]
     */
    public function filterCards(array $targets): array
    {
        $filter = fn ($card) => !$this->isInverted === ($card->getElement() !== $this->searchedElement);

        return $targets;
    }
}
