<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\FilterOperator;

/**
 * Filter any Target by their stat
 */
class StatFilter extends TargetFilter
{
    private DinocardStat $stat;
    private FilterOperator $operator;
    private int $value;

    /**
     * @param DinocardStat   $stat
     * @param FilterOperator $operator
     * @param int            $value
     */
    public function __construct(DinocardStat $stat, FilterOperator $operator, int $value)
    {
        $this->stat = $stat;
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * @param array $targets
     *
     * @return array|Dinoz[]
     */
    public function filterDinoz(array $targets): array
    {
        if (count($targets) === 0) {
            return $targets;
        }

        if ($this->operator === FilterOperator::Maximum) {
            $selectedTargets = array($targets[0]);
            foreach ($targets as $target) {
                $statDiff = $target->getStat($this->stat) - $selectedTargets[0]->getStat($this->stat);
                if ($statDiff > 0) {
                    $selectedTargets = array($target);
                } else {
                    if (0 === $statDiff) {
                        $selectedTargets[] = $target;
                    }
                }
            }

            return $selectedTargets;
        }

        if ($this->operator === FilterOperator::Minimum) {
            $selectedTargets = array($targets[0]);
            foreach ($targets as $target) {
                $statDiff = $target->getStat($this->stat) - $selectedTargets[0]->getStat($this->stat);
                if ($statDiff < 0) {
                    $selectedTargets = array($target);
                } else {
                    if (0 === $statDiff) {
                        $selectedTargets[] = $target;
                    }
                }
            }

            return $selectedTargets;
        }

        if ($this->operator === FilterOperator::GreaterOrEquals) {
            $filter = fn($dinoz) => $dinoz->getStat($this->stat) >= $this->value;
        }
        if ($this->operator === FilterOperator::LowerOrEquals) {
            $filter = fn($dinoz) => $dinoz->getStat($this->stat) <= $this->value;
        }

        return array_filter($targets, $filter);
    }

    /**
     * @param array $targets
     *
     * @return array|Card[]
     */
    public function filterCards(array $targets): array
    {
        assert("Cards can't be filtered by stat");
        // Should we have some stat on card? (Cost, ...)

        return $targets;
    }
}
