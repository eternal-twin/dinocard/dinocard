<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\Keyword;

/**
 * Filter any Target by their Keyword
 */
class KeywordFilter extends TargetFilter
{
    private bool $isInverted;
    private Keyword $searchedKeyword;

    /**
     * @param Keyword $searchedKeyword
     * @param bool    $inverted
     */
    public function __construct(Keyword $searchedKeyword, bool $inverted = false)
    {
        $this->searchedKeyword = $searchedKeyword;
        $this->isInverted = $inverted;
    }

    /**
     * @param array $targets
     *
     * @return array|Dinoz[]
     */
    public function filterDinoz(array $targets): array
    {
        $filter = fn($dinoz) => !$this->isInverted === $dinoz->getOwningCard()->hasKeyword($this->searchedKeyword);

        return array_filter($targets, $filter);
    }

    /**
     * @param array $targets
     *
     * @return array|Card[]
     */
    public function filterCards(array $targets): array
    {
        $filter = fn($card) => !$this->isInverted === $card->hasKeyword($this->searchedKeyword);

        return array_filter($targets, $filter);
    }
}