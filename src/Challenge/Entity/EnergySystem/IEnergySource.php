<?php

namespace App\Challenge\Entity\EnergySystem;

use App\Challenge\Enum\ElementType;

interface IEnergySource
{
    /**
     * Which element does this energy source returns?
     * @return ElementType
     */
    public function getEnergyElement(): ElementType;
}