<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\ElementType;

class CardCost
{
    private ElementType $element;
    private int $count;

    /**
     * @param ElementType $element
     * @param int $cost
     */
    public function __construct(ElementType $element, int $cost)
    {
        $this->element = $element;
        $this->count = $cost;
    }

    /**
     * @return ElementType
     */
    public function getElement() : ElementType
    {
        return $this->element;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}