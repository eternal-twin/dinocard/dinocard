<?php

namespace App\Challenge\Service;

use App\Challenge\Entity\Opponent;
use App\Entity\CardDB;
use App\Entity\DeckDB;

class OpponentFactory
{
    private CardFactory $cardFactory;

    /**
     * @param CardFactory $cardFactory
     */
    public function __construct(CardFactory $cardFactory)
    {
        $this->cardFactory = $cardFactory;
    }

    /**
     * @param string $name
     * @param int[]  $cardList
     * @param int    $deckSize
     *
     * @return Opponent
     */
    public function generateOpponent(string $name, array $cardList = [], int $deckSize = 20) : Opponent
    {
        /** @var array<int> $availableId */
        $availableId = $this->cardFactory->getAllCardId();

        $fightOpponent = new Opponent($name);

        foreach ($cardList as $cardID) {
            if (in_array($cardID, $availableId)) {
                $newCard = $this->cardFactory->loadCard($cardID);
                $fightOpponent->getDeck()->addCard($newCard);
            }
        }
        for ($i = count($cardList); $i < $deckSize; $i++) {
            $newCard = $this->cardFactory->loadCard($availableId[rand(0, count($availableId) - 1)]);
            $fightOpponent->getDeck()->addCard($newCard);
        }

        return $fightOpponent;
    }

    /**
     * @param DeckDB $deckDB
     *
     * @return Opponent
     */
    public function loadOpponentFromDeck(DeckDB $deckDB): Opponent
    {
        $fightOpponent = new Opponent($deckDB->getOwner()->getDisplayName());
        $fightOpponent->setDeckId($deckDB->getId());

        /** @var CardDB $cardInfo */
        foreach ($deckDB->getCards() as $cardInfo) {
            for ($i = 0; $i < $cardInfo->getAmount(); $i++) {
                $newCard = $this->cardFactory->loadCard($cardInfo->getCardId());
                $fightOpponent->getDeck()->addCard($newCard);
            }
        }

        return $fightOpponent;
    }
}
