<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum DeckPreset: string
{
    case Empty = "empty";

    // News
    case SpellsBatch2 = "Second Batch of Spells";
    case DebugSpells = "Dummy Spells For most effects";

    // Player deck list:
    case PlayerDeck_Gil = "Ombre d'Ambragon (Bois)"; // Provided by Gilg248

    case PlayerDeck_Rayrton = "Test tri";

    //Keyword Centric Presets
    case DischargeTest = "Discharge Test";
    case ArmorTest = "Armor Test";
    case AssassinTest = "Assassin Test";
    case BerserkTest = "Berserk Test";
    case BubbleTest = "Bubble Test";
    case BroodTest = "NYI-Brood Test";
    case OverflowTest = "Overflow Test";
    case PoisonedTest = "Poisoned Test";
    case EphemeralTest = "Ephemeral Test";
    case EternalTest = "Eternal Test";
    case FocusTest = "Focus Test";
    case HerosTest = "Heros Test";
    case InitiativeTest = "Initiative Test";
    case PackTest = "Pack Test";
    case CowardTest = "Coward Test";
    case StompyTest = "Stompy Test";
    case RegenerationTest = "Regeneration Test";
    case RockTest = "Rock Test";
    case WizardTest = "Wizard Test";
    case SurvivorTest = "Survivor Test";
    case TenaciousTest = "Tenacious Test";
    case VaporousTest = "Vaporous Test";
    case VenomousTest = "Venomous Test";
    case FlyingTest = "Flying Test";

    case DinozTargetingTest = "dinozTargetingTest";
    case StartTurnKeywordTest = "startTurnKeywordTest";
    case ShuffleGraveyardTest = "Shuffle Graveyard Test";

    case CharmsTest = "Charms Test";
    case DeckCounterTest = "The Black Hole";

    case TargetSpells = "Filter Targets Test";
    case ResurrectionTest = "Resurrection Test";
    case GainControlTest = "Gain Control Test";
    case AntiOctoboon = "Anti-Octoboon";
    case Octoboon = "Octoboon";
    case DinozTrigger = "DinozTrigger";

    /**
     * @return array|int[]
     */
    public function deckList(): array
    {
        return match ($this) {
            self::Empty => [],

            self::PlayerDeck_Gil => [156, 156, 156, 157, 157, 157, 159, 159, 159, 161, 161, 161, 331, 331, 331, 426, 426, 426, 531, 531],
            self::PlayerDeck_Rayrton => [517, 517, 437, 437, 437, 244, 244, 244, 317, 317, 317, 307, 307, 307, 468, 468, 468, 453, 453, 453, 407, 407, 407, 358, 358],

            self::DischargeTest => [348, 348, 348, 348, 457, 457, 457, 457],
            self::ArmorTest => [159, 159, 164, 164, 228, 228, 254, 254, 351, 351, 436, 436, 468, 468],
            self::AssassinTest => [522, 522, 522, 522, 522, 522, 522, 522, 535, 535, 535, 535, 535, 535, 535, 535],
            self::BerserkTest => [428, 428, 428, 428, 428, 441, 441, 441, 441, 441],
            self::BubbleTest => [517, 517, 517, 517, 517, 517, 517, 517, 517, 517, 517, 517],
            self::BroodTest => [292, 292, 292, 292, 352, 352, 352, 352],
            self::OverflowTest => [156, 156, 157, 157, 159, 159, 407, 407, 426, 426, 531, 531, 531],
            self::PoisonedTest => [347, 347, 347, 347, 347, 347],
            //self::StuckTest => [], // None in the set
            self::EphemeralTest => [185, 185, 185, 185, 185, 185, 185],
            self::EternalTest => [456, 456, 456, 457, 457, 457],
            self::FocusTest => [185, 185, 185, 356, 356, 356, 451, 451, 451],
            self::HerosTest => [322, 322, 424, 424, 436, 436, 449, 449, 504, 504, 517, 517, 522, 522],
            self::InitiativeTest => [325, 325, 325, 352, 352, 352, 449, 449, 449, 504, 504, 504],
            self::PackTest => [216, 216, 219, 219, 322, 322, 323, 323, 441, 441],
            self::CowardTest => [426, 426, 426, 426, 426, 440, 440, 440, 440, 440],
            self::StompyTest => [318, 318, 319, 319, 321, 321, 323, 323, 363, 363],
            self::RegenerationTest => [220, 220, 224, 224, 228, 228, 257, 257, 322, 322, 522, 522],
            self::RockTest => [257, 257, 257, 263, 263, 263, 407, 407, 407],
            self::WizardTest => [288, 288, 291, 291, 351, 351, 407, 407, 424, 424],
            self::SurvivorTest => [978, 978, 978, 133, 133, 133, 192, 192, 414, 414, 415, 415],
            self::TenaciousTest => [139, 139, 193, 193, 321, 321, 322, 322, 324, 324, 504, 504, 536, 536],
            self::VaporousTest => [298, 298, 352, 352, 444, 444, 466, 466],
            self::VenomousTest => [222, 222, 222, 351, 351, 351, 361, 361, 361],
            self::FlyingTest => [142, 185, 285, 288, 289, 292, 295, 300, 420, 457, 468, 469],

            self::DinozTargetingTest => [142, 216, 219, 263, 285, 289, 295, 298, 300, 318, 319, 323, 363, 420, 444, 466, 469],
            self::StartTurnKeywordTest => [185, 220, 224, 257, 347],
            self::ShuffleGraveyardTest => [156, 156, 156, 156, 156, 156, 392, 392, 392, 392, 392, 392, 392, 392, 392, 392],

            self::CharmsTest => [220, 220, 220, 220, 220, 170, 170, 180, 180, 180, 148, 148, 148, 148, 148, 537, 537, 537, 537, 537],
            self::DeckCounterTest => [306, 306, 306, 306, 306, 306, 306, 279, 279, 279, 279, 279, 279, 279, 330, 330, 330, 330, 330, 330],

            self::DebugSpells => [995, 994, 993, 992, 991, 990, 988, 987, 986, 985, 984, 983, 982, 981, 980, 979],
            self::SpellsBatch2 => [134, 145, 146, 147, 154, 167, 168, 204, 206, 239, 240, 243, 244, 271, 272, 273, 275, 279, 304, 305, 306, 307, 326, 338, 358, 378, 379, 383, 477, 511],

            self::TargetSpells => [326, 273, 488, 354, 478, 493, 206, 358, 303, 168],
            self::ResurrectionTest => [252, 252, 252, 252, 252, 252, 252, 449, 449, 449, 491, 491, 491, 491, 491, 491, 491, 491, 491, 491],
            self::GainControlTest => [234, 234, 236, 236, 236, 289, 289, 289, 300, 300, 300, 317, 317, 317, 330, 330, 330, 537, 537, 537],
            self::AntiOctoboon => [973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973, 973],
            self::Octoboon => [449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449, 449],
            self::DinozTrigger => [969, 969, 969, 969, 968, 968, 968, 968, 967, 967, 967, 967, 966, 966, 966, 966, 965, 965, 965, 965],

        };
    }

    /**
     * @return DeckPreset
     */
    public static function getRandom(): DeckPreset
    {
        $deckPresets = DeckPreset::cases();

        return $deckPresets[rand(1, count($deckPresets) - 1)]; // Can be everything except empty!;
    }
}
