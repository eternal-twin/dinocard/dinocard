<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum RoundPhase: string
{
    case Start  = "start";
    case OpponentA = "opponentA";
    case OpponentB = "opponentB";
    case Attack = "attack";
    case End = "end";

    /**
     * @param RoundPhase $currentPhase
     *
     * @return RoundPhase
     */
    public static function nextPhase(RoundPhase $currentPhase): RoundPhase
    {
        $cases = RoundPhase::cases();
        $names = array_column($cases, 'name');
        $nextPhaseIndex = (1 + array_search($currentPhase->name, $names)) % count($names);

        return $cases[$nextPhaseIndex];
    }

    /**
     * @param RoundPhase $currentPhase
     *
     * @return RoundPhase
     */
    public static function previousPhase(RoundPhase $currentPhase): RoundPhase
    {
        $cases = RoundPhase::cases();
        $names = array_column($cases, 'name');
        $previousPhaseIndex = (-1 + array_search($currentPhase->name, $names)) % count($names);

        return $cases[$previousPhaseIndex];
    }
}
