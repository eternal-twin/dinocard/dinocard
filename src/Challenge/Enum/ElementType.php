<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum ElementType: string
{
    case Fire      = "fire";
    case Wood      = "wood";
    case Water     = "water";
    case Thunder   = "thunder";
    case Air       = "air";
    case Neutral   = "neutral";
}