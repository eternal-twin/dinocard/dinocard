<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum FilterOperator: string
{
    case Maximum  =  "maximum";
    case Minimum  =  "minimum";
    case GreaterOrEquals = "greaterOrEquals";
    case LowerOrEquals = "lowerOrEquals";
}