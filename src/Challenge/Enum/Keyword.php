<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum Keyword: string
{
    case Discharge = "discharge"; //Arc
    case Armor = "armor"; //Armure
    case Assassin = "assassin"; //Assassin
    case Berserk = "berserk"; //Berserk
    case Bubble = "bubble"; //Bulle
    case Brood = "brood"; //Couvée
    case Overflow = "overflow"; //Débordement
    case Poisoned = "poisoned"; //Empoisonné
    case Stuck = "stuck"; //Englué
    case Ephemeral = "ephemeral"; //Ephémère
    case Eternal = "eternal"; //Eternel
    case Focus = "focus"; //Focus
    case Hero = "hero"; //Héros
    case Initiative = "initiative"; //Initiative
    case Pack = "pack"; //Meute
    case Coward = "coward"; //Peureux
    case Stompy = "stompy"; //Piétinement
    case Regeneration = "regeneration"; //Régénération
    case Rock = "rock"; //Rock
    case Wizard = "wizard"; //Sorcier
    case Survivor = "survivor"; //Survivant
    case Tenacious = "tenacious"; //Tenace
    case Vaporous = "vaporous"; //Vaporeux
    case Venomous = "venomous"; //Venimeux
    case Flying = "flying"; //Vol
}