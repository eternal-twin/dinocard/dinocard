<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum Trigger: string
{
    case onHatch  = "onHatch";
    case onDeath = "onDeath";
    case onAttackEnd = "onAttackEnd";
    case onAllyStartTurn = "onAllyStartTurn";
    case onEnemyStartTurn = "onEnemyStartTurn";
}