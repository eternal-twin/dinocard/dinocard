<?php

namespace App\Controller;

use App\Challenge\Service\CardFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends AbstractController
{
    private CardFactory $cardFactory;

    /**
     * @param CardFactory $cardFactory
     */
    public function __construct(CardFactory $cardFactory)
    {
        $this->cardFactory = $cardFactory;
    }

    /**
     * @Route("/card/view/{id}", name="viewCard")
     *
     * @param int $id
     *
     * @return Response
     */
    public function viewCard(int $id): Response
    {
        if ($this->cardFactory->isValidCardId($id)) {
            $params = ['cardId' => $id];

            return $this->render('Card/viewCard.html.twig', $params);
        }

        return $this->redirectToRoute('homepage', [], 404);
    }
}