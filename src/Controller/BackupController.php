<?php

namespace App\Controller;

use App\Entity\DinocardBackup;
use App\Repository\BackupRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackupController extends AbstractController
{
    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    /**
     * @Route("/api/backup/save", name="saveAccountData", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function saveAccountData(Request $request): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        // Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');

        if (!str_starts_with($request->headers->get('Content-Type'), 'application/json')) {
            $response->setContent("Request doesn't provided Json");
            $response->setStatusCode(400);
        } else {
            $json = json_decode($request->getContent(), true);
            $rawJson = $request->getContent();

            $userIdKey = 'playerId';
            $usernameKey = 'playerName';
            if (false === array_key_exists($userIdKey, $json)) {
                $response->setContent("Json doesn't contain the field playerId");
                $response->setStatusCode(400);
            } elseif (false === array_key_exists($usernameKey, $json)) {
                $response->setContent("Json doesn't contain the field playerName");
                $response->setStatusCode(400);
            } else {
                $userId = $json[$userIdKey];
                $username = $json[$usernameKey];
                $backup = new DinocardBackup($userId, $username, $rawJson);

                $this->managerRegistry->getManager()->persist($backup);
                $this->managerRegistry->getManager()->flush();

                $response->setContent("Backup saved :)");
                $response->setStatusCode(200);
            }
        }

        return $response;
    }

    /**
     * @Route("/admin/backup/", name="adminBackup")
     *
     * @return Response
     */
    public function adminBackup(): Response
    {
        /** @var BackupRepository $backupRepository */
        $backupRepository = $this->managerRegistry->getRepository(DinocardBackup::class);
        $allBackups = $backupRepository->getAllBackupAccount();

        $params = [
            'allBackups' => $allBackups,
        ];

        return $this->render('Admin/adminBackup.html.twig', $params);
    }

    /**
     * @Route("/backup/{userId}", name="backupView")
     *
     * @param int $userId
     *
     * @return Response
     */
    public function backupView(int $userId): Response
    {
        /** @var BackupRepository $backupRepository */
        $backupRepository = $this->managerRegistry->getRepository(DinocardBackup::class);
        $myBackups = $backupRepository->getBackupData($userId);

        $params = [
            'myBackups' => $myBackups,
        ];

        return $this->render('Onboarding/myBackupData.html.twig', $params);
    }

    // TODO View to show list of account that are not linked...
    // TODO Request to link account
}
