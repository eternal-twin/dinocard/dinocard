<?php

namespace App\Controller;

use App\Challenge\Enum\DeckPreset;
use App\Entity\News;
use App\Entity\User;
use App\Repository\NewsRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// TODO Move News actions to NewsController

class AdminController extends AbstractController
{
    private ManagerRegistry $managerRegistry;

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @Route("/admin", name="admin")
     *
     * @return Response
     */
    public function adminIndex(): Response
    {
        $params = array();

        return $this->render('Admin/admin.html.twig', $params);
    }

    /**
     * @Route("/admin/news/", name="adminNews")
     *
     * @return Response
     */
    public function adminNews(): Response
    {
        // TODO Probably limit that?
        /** @var NewsRepository $newsRepository */
        $newsRepository = $this->managerRegistry->getRepository(News::class);
        $allNews = $newsRepository->findBy([], ['createdAt' => 'DESC']);

        $params = [
            'news' => $allNews,
            'newsToEdit' => null,
        ];

        return $this->render('Admin/adminNews.html.twig', $params);
    }

    /**
     * @Route("/admin/news/edit/{newsId}", name="adminEditNews")
     *
     * @param int $newsId news to edit
     *
     * @return Response
     */
    public function adminNewsEdit(int $newsId): Response
    {
        // TODO Probably limit that?
        /** @var NewsRepository $newsRepository */
        $newsRepository = $this->managerRegistry->getRepository(News::class);
        $allNews = $newsRepository->findBy([], ['createdAt' => 'DESC']);
        $newsToEdit = $newsRepository->find($newsId);

        $params = [
            'news' => $allNews,
            'newsToEdit' => $newsToEdit,
        ];

        return $this->render('Admin/adminNews.html.twig', $params);
    }

    /**
     * Create the news with the specified info.
     * @Route("/admin/news/create", name="adminCreateNews", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function adminNewsCreate(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $title = $request->get('title');
        $content = $request->get('content');
        $picture = $request->get('img');

        if (!empty($title) && !empty($content)) {
            $manager = $this->managerRegistry->getManager();
            $news = new News($user, $title, $content, $picture);
            $manager->persist($news);
            $manager->flush();
        }

        return $this->redirectToRoute('adminNews', ['newsIdToModify' => -1]);
    }

    /**
     * Edit the news with the specified info.
     * @Route("/admin/news/update", name="adminUpdateNews", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function adminNewsUpdate(Request $request): Response
    {
        $id = $request->get('newsId');
        $title = $request->get('title');
        $content = $request->get('content');
        $picture = $request->get('img');

        if (!empty($title) && !empty($content) && !empty($id)) {
            /** @var NewsRepository $newsRepository */
            $newsRepository = $this->managerRegistry->getRepository(News::class);
            $news = $newsRepository->find($id);

            $news->setTitle($title);
            $news->setContent($content);
            $news->setPicture($picture);
            $news->setIsPublished(false);

            $manager = $this->managerRegistry->getManager();
            $manager->persist($news);
            $manager->flush();
        }

        return $this->redirectToRoute('adminNews');
    }

    /**
     * @Route("/admin/news/toggle/{newsId}", name="adminNewsToggle")
     *
     * @param int $newsId
     *
     * @return Response
     */
    public function adminNewsToggle(int $newsId): Response
    {
        /** @var NewsRepository $newsRepository */
        $newsRepository = $this->managerRegistry->getRepository(News::class);
        $news = $newsRepository->findOneBy(['id' => $newsId]);
        $news->setIsPublished(!$news->isPublished());

        $manager = $this->managerRegistry->getManager();
        $manager->flush();

        return $this->redirectToRoute('adminNews');
    }

    /**
     * @Route("/admin/news/delete/{newsId}", name="adminNewsDelete")
     *
     * @param int $newsId
     *
     * @return Response
     */
    public function adminNewsDelete(int $newsId): Response
    {
        /** @var NewsRepository $newsRepository */
        $newsRepository = $this->managerRegistry->getRepository(News::class);
        $news = $newsRepository->findOneBy(['id' => $newsId]);

        $manager = $this->managerRegistry->getManager();
        $manager->remove($news);
        $manager->flush();

        return $this->redirectToRoute('adminNews');
    }

    /**
     * List all cards currently implemented
     * @Route("/debugAllCards", name="debugAllCards")
     *
     * @return Response
     */
    public function debugAllCards(): Response
    {
        return $this->render('Admin/debugAllCards.html.twig');
    }

    /**
     * @Route("/deckPresetsFightSelection", name = "deckPresetsSelector")
     *
     * @return Response
     */
    public function deckPresetsFightSelection(): Response
    {
        $params = array();

        $params['presets'] = [];

        foreach (DeckPreset::cases() as $preset) {
            array_push($params['presets'], $preset->value);
        }

        return $this->render('Admin/presetFightSelector.html.twig', $params);
    }

    /**
     * @Route("/deckPresetsFightSelectionConfirmed", methods={"POST"}, name = "confirmDeckPresetsForFight")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function deckPresetsFightSelectionConfirmed(Request $request): Response
    {
        $presetA = $request->request->get('deckPresetA');
        $presetB = $request->request->get('deckPresetB');

        return $this->redirectToRoute('fightWithPreset', ['presetA' => $presetA, 'presetB' => $presetB]);
    }

    /**
     * @Route("/admin/error", name="adminError")
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function AdminExceptionTest(): Response
    {
        throw new \Exception('Erreur test!');
    }

    /**
     * @return bool
     */
    private function isAdmin() : bool
    {
        return $this->getUser()->getRoles()[0] === "ROLE_ADMIN";
    }
}