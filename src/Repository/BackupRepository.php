<?php

namespace App\Repository;

use A\B;
use App\Entity\DinocardBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DinocardBackup>
 *
 * @method DinocardBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DinocardBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DinocardBackup[]    findAll()
 * @method DinocardBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DinocardBackup::class);
    }

    public function getAllBackupAccount(): mixed
    {
        return $this->createQueryBuilder('b')
            ->select('b.userId, count(b.userId) as count, b.username')
            ->groupBy('b.userId, b.username')
            ->orderBy('b.userId')
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    public function getBackupData(int $id)
    {
        return $this->createQueryBuilder('b')
            ->select('b.id, b.data')
            ->where('b.userId = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}