<?php

namespace App\Repository;

use App\Entity\DeckDB;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DeckDB>
 *
 * @method DeckDB|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeckDB|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeckDB[]    findAll()
 * @method DeckDB[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeckDB::class);
    }
}
