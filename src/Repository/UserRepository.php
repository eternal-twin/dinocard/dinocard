<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $role
     *
     * @return float|int|mixed|string
     */
    public function findAllByRole(string $role)
    {
        return $this->createQueryBuilder('u')
            ->where("JSON_GET_TEXT(u.roles,0) = :role")
            ->setParameter('role', $role)
            ->getQuery()
            ->getResult();
    }

    /**
     * Returns a page of users sorted by points
     * TODO Ideally points should be computed each time a player create a deck or level up!
     *
     * @param int $page
     * @param int $countPerPage
     * @return float|int|mixed|string
     */
    public function getLeaderboardPage(int $page, int $countPerPage)
    {
        $query = $this->createQueryBuilder('user')
            ->select('user, count(deck.id) as point')
            ->leftJoin('user.decks', 'deck', Join::WITH, 'deck.owner = user.id')
            ->groupBy('user.id')
            ->orderBy('point', 'DESC')
            ->setFirstResult(($page - 1) * $countPerPage)
            ->setMaxResults($countPerPage)
            ->getQuery();

        return $query->getResult();
    }
}
