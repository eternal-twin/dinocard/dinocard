<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dinocard account backup
 * @ORM\Entity(repositoryClass=App\Repository\BackupRepository::class)
 * @ORM\Table(name="backup")
 */
class DinocardBackup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * Original Dinocard id
     * @ORM\Column(type="integer")
     */
    private int $userId;

    /**
     * @ORM\Column(type="string")
     */
    private string $username;

    /**
     * @ORM\Column(type="json")
     */
    private mixed $data;

    /**
     * Many BackupData can have one BackupLink. This is the owning side.
     * @ORM\ManyToOne(targetEntity="DinocardBackupLink", inversedBy="allBackupData")
     */
    private ?DinocardBackupLink $backupLink;

    /**
     * @param int    $userId
     * @param string $username
     * @param $json
     */
    public function __construct(int $userId, string $username, $json)
    {
        $this->userId = $userId;
        $this->username = $username;
        $this->data = $json;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getData(): mixed
    {
        return $this->data;
    }

    /**
     * @param DinocardBackupLink $backupLink
     */
    public function setBackupLink(DinocardBackupLink $backupLink): void
    {
        // Already linked to someone...
        assert($this->backupLink !== null);
        $this->backupLink = $backupLink;
    }
}