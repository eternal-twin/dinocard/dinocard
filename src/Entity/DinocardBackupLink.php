<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Keep track of who linked DinocardData to which User.
 * @ORM\Entity(repositoryClass=BackupLinkRepository::class)
 * @ORM\Table(name="backupLink")
 */
class DinocardBackupLink
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * Who owned this account backup?
     * @ORM\OneToOne(targetEntity="User")
     */
    private User $owner;

    /**
     * One BackupLink has many Backup. This is the inverse side.
     * @ORM\OneToMany(targetEntity="DinocardBackup", mappedBy="backupLink")
     *
     * @var Collection<DinocardBackup>
     */
    private Collection $allBackupData;

    /**
     * Does this account link has been validated by an admin?
     * @ORM\Column(type="boolean")
     */
    private bool $validated = false;

    /**
     * @param User       $owner
     * @param Collection $allBackupData
     */
    public function __construct(User $owner, Collection $allBackupData)
    {
        $this->owner = $owner;
        $this->allBackupData = $allBackupData;

        /** @var DinocardBackup $backupData */
        foreach ($this->$allBackupData as $backupData) {
            $backupData->setBackupLink($this);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getData(): Collection
    {
        return $this->allBackupData;
    }

    /**
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @return bool
     */
    public function isValidated(): bool
    {
        return $this->validated;
    }

    /**
     * @param bool $validated
     */
    public function setValidated(bool $validated): void
    {
        $this->validated = $validated;
    }
}