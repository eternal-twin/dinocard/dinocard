<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Entry point for User creation / (re)connection
 */
class UserService implements UserServiceInterface
{
    private string $appEnv;
    private EntityManagerInterface $entityManager;

    private UserRepository $repository;

    /**
     * @param string                 $appEnv
     * @param EntityManagerInterface $entityManager
     * @param UserRepository         $repository
     */
    public function __construct(string $appEnv, EntityManagerInterface $entityManager, UserRepository $repository)
    {
        $this->appEnv = $appEnv;
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function save(User $user): User
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param string $userId
     *
     * @return User|null
     */
    public function findUserByUserId(string $userId): ?User
    {
        return $this->repository->findOneBy(['userId' => $userId]);
    }

    /**
     * @param string $userId
     * @param string $displayName
     * @param bool   $isAdmin
     *
     * @return User
     */
    public function createUser(string $userId, string $displayName, bool $isAdmin = false): User
    {
        $user = new User($userId, $displayName);
        if ($isAdmin) {
            $user->setRoles(['ROLE_ADMIN']);
        }
        if ($this->appEnv === 'dev') {
            $user->setRoles(['ROLE_ADMIN']);
        }

        $this->save($user);

        return $user;
    }

    /**
     * @param User $user
     *
     * @return void
     */
    public function reconnect(User $user): void
    {
        $user->setLastConnectionDate(new \DateTime());
        $this->save($user);
    }
}
