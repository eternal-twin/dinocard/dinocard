<?php

namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;

class PreflightIgnoreOnNewRelicListener
{
    /**
     * @param ResponseEvent $event
     * @return void
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        if (!extension_loaded('newrelic')) {
            return;
        }

        if ('OPTIONS' === $event->getRequest()->getMethod()) {
            newrelic_ignore_transaction();
        }
    }
}