<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231001133421 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Account Backup logic';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE backup_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE backupLink_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE backup (id INT NOT NULL, backup_link_id INT DEFAULT NULL, data JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3FF0D1AC1F592F70 ON backup (backup_link_id)');
        $this->addSql('CREATE TABLE backupLink (id INT NOT NULL, owner_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BFB203F57E3C61F9 ON backupLink (owner_id)');
        $this->addSql('ALTER TABLE backup ADD CONSTRAINT FK_3FF0D1AC1F592F70 FOREIGN KEY (backup_link_id) REFERENCES backupLink (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE backupLink ADD CONSTRAINT FK_BFB203F57E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE deck ALTER private DROP DEFAULT');
        $this->addSql('ALTER TABLE deck ALTER private SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE backup_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE backupLink_id_seq CASCADE');
        $this->addSql('ALTER TABLE backup DROP CONSTRAINT FK_3FF0D1AC1F592F70');
        $this->addSql('ALTER TABLE backupLink DROP CONSTRAINT FK_BFB203F57E3C61F9');
        $this->addSql('DROP TABLE backup');
        $this->addSql('DROP TABLE backupLink');
        $this->addSql('ALTER TABLE deck ALTER private SET DEFAULT false');
        $this->addSql('ALTER TABLE deck ALTER private DROP NOT NULL');
    }
}
