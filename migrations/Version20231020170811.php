<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231020170811 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Account backup fixes';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE backup ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE backup ADD username VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE backuplink ADD validated BOOLEAN NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE backupLink DROP validated');
        $this->addSql('ALTER TABLE backup DROP user_id');
        $this->addSql('ALTER TABLE backup DROP username');
    }
}
