Dinocard+ Setup
-------------------------------

Dinocard+ est un projet OpenSource de reconstruction du jeu Dinocard développé par MotionTwin.
Il utilise le framework PHP Symfony dans sa version 5.4 et PHP 8.2 !

---

# Lancer le serveur WEB en local: Quickstart

Afin de pouvoir lancer et tester le site, il faut d'abord procéder à quelques installations.

Rendez vous d'abord sur cette page pour avoir accès à la documentation complète d'un setup Symfony (https://symfony.com/doc/5.4/setup.html)

1. **Technologies à installer** (Technical Requirements)

Veillez à bien installer Php, Composer, Symfony & yarn. Et enfin Postgres! (Vous pouvez utiliser docker pour postgres si vous connaissez)  
Si la commande `symfony check:requirements` indique Succès, vous pouvez continuer.  
Une fois php installé, veuillez modifier le fichier php.ini et décommenter la ligne `extension=pdo_pgsql` (retirer le ';' )

2. **Charger les plugins tierces non stockés sur le git**

Rendez vous dans le dossier Dinocard de ce projet git et lancez la commande : `composer install`  
Ceci à pour but de télécharger et installer les plugins non stockés sur le git du fait de leurs poids.

3. **Modifier vos variables d'environnement**

Rendez-vous dans le dossier Dinocard.  
Copiez le fichier **.env.example** et renommez le **.env.local** puis décommentez et modifiez la ligne 21 (DATABASE_URL pour postgres) à votre convenance (à savoir : modifiez `db_user` et `db_password` selon la config de votre bdd).  
Remplacez aussi `db_name` par `Dinocard`

4. **Lancement de Eternaltwin** (nécessaire pour l'utilisation de compte / OAuth2).

Pour installer Eternaltwin et ses dépendances : `yarn install`  
Si le fichier **etwin.toml** n'existe pas, créez-le en copiant **etwin.toml.example**.
Lancez ensuite la commande `yarn etwin db create`. Note: Si vous ne voulez pas utiliser de base de données, commentez la ligne 10 (api = "postgres") et décommentez la ligne 11 (api = "in-memory"). 

Puis lancer l'application : `yarn etwin start`

Vous pouvez maintenant accéder Eternaltwin (lite) depuis http://localhost:50320

5. **Créer/Mettre à jour la base de données**

Rendez-vous dans le dossier Dinocard et lancez les commandes suivantes en entrant yes quand demandé :  
`php bin/console doctrine:database:create`  
`php bin/console doctrine:migrations:migrate`  
`php bin/console doctrine:fixtures:load`  

6. **Lancement du serveur**

Rendez-vous dans le dossier Dinocard et lancez le projet Symfony à l'aide de la commande :
`symfony server:start` 

Rendez vous maintenant sur http://localhost:8000 (ou bien http://localhost:8080) et profitez du site 💖

Vous allez devoir vous créer un compte etwin (pas de panique, ceci n'est qu'en local, vous pouvez mettre n'importe quoi pour les creds).  
Si vous rencontrez des problèmes lors de la connection, n'hésitez pas à déco reco et/ou à relancer le serveur de dév.

# Configurer Eternaltwin:

Etwin documentation: https://eternal-twin.net/docs/app/etwin-oauth  
dans Dinoparc/.env: (ou .env.local)
```
    IDENTITY_SERVER_URI="http://localhost:50320"
    OAUTH_CALLBACK="'http://localhost:8000/oauth/callback'" 
    OAUTH_AUTHORIZATION_URI="http://localhost:50320/oauth/authorize"
    OAUTH_TOKEN_URI="http://localhost:50320/oauth/token"
    OAUTH_CLIENT_ID="dinoparc@clients"
    OAUTH_SECRET_ID="dev_secret"
```
Ces variables d'environnements doivent correspondre à celles définis dans EternalTwin/etwin.toml
```
[clients.dinoparc]
display_name = "dinoparc"
app_uri = "http://localhost:8000"
callback_uri = "http://localhost:8000/oauth/callback"
secret = "dev_secret"
```

## Utiliser Eternaltwin avec une database (api = postgres uniquement)

Etwin documentation: https://eternal-twin.net/docs/app/etwin-integration

Il faut au prealable creer une database pour eternaltwin.
Dans le fichier etwin.toml changer les parametres suivants pour qu'il corresponde a votre database
```
api = "postgres"
[db]
# Database service host
host = "localhost"
# Database service port
port = 5432
# Database name
name = "eternal_twin"
# Database user (role). In development, it is recommended to be a superuser to manage extensions.
user = "user"
# Password for the database user.
password = "password"
```

Vous pouvez maintenant initialiser la base de données :
```
yarn etwin db create
```

Si vous avez une base de données existante, vous pouvez la mettre à jour avec :

```
yarn etwin db upgrade
```

# License

[AGPL-3.0-or-later](./license.md)
